FROM ruby:2.7-alpine

COPY pipe /
COPY LICENSE.txt pipe.yml README.md /

RUN chmod a+x /*.rb

ENTRYPOINT ["irb", "--noecho", "--noverbose", "/pipe.rb"]
