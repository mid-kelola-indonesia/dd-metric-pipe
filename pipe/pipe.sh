#!/usr/bin/env bash
#
# This pipe is an example to show how easy is to create pipes for Bitbucket Pipelines.
#

# source "$(dirname "$0")/common.sh"

BITBUCKET_REPOSITORY=REPOSITORY_NAME
METRIC_KEY="$METRIC_KEY"
METRIC_SCORE="$METRIC_SCORE"
DD_TAGS='"branch:'"$BITBUCKET_BRANCH"'","repository:'"$BITBUCKET_REPOSITORY"'"'

if [ ! -z "$TAGS" ]
then
  DD_TAGS+=','$TAGS''
fi

# curl --location --request POST 'https://api.datadoghq.com/api/v1/series?api_key='"$DD_API_KEY" \
curl --location --request POST 'https://webhook.site/6cff9ff1-d4bc-43ab-b9ef-24735152dc00' \
--header 'Content-Type: application/json' \
--data-raw '{
    "series": [
        {
            "metric": "'"$METRIC_KEY"'",
            "points": [
                [
                    "'$(date +"%s")'",
                    "'"$METRIC_SCORE"'"
                ]
            ],
            "host": "bitbucket.org",
            "interval": null,
            "tags": ['"$DD_TAGS"'],
            "type": "gauge"
        }
    ]
}'
