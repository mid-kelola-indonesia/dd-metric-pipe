#!/usr/bin/env irb

require 'net/http'
require 'uri'
require 'json'
require 'date'

begin
  dd_api_key = ENV['API_KEY']
  bitbucket_branch = ENV['BITBUCKET_BRANCH']
  bitbucket_repository = ENV['REPOSITORY_NAME'].nil? ? ENV['BITBUCKET_REPO_SLUG'] : ENV['REPOSITORY_NAME']

  input_series = ENV['DD_METRICS']

  raise 'Datadog Metrics is required' if input_series.nil?

  input_series_json = JSON.parse(input_series)

  series = []
  default_tags = ["branch:#{bitbucket_branch}", "repository:#{bitbucket_repository}"]

  input_series_json.each do |seri|
    tags = default_tags
    tags = default_tags + seri['tags'] unless seri['tags'].nil?

    series << {
      metric: seri['metric'].to_s,
      points: [
        [
          Time.now.to_i,
          seri['score'].to_s
        ]
      ],
      host: "bitbucket.org",
      interval: nil,
      tags: tags,
      type: "gauge"
    }
  end

  payload = {series: series}.to_json

  response = Net::HTTP.post URI("https://api.datadoghq.com/api/v1/series?api_key=#{dd_api_key}"),
            payload,
            "Content-Type" => "application/json"

  raise "POST Request failed: #{response.code}" unless response.code.eql? '202'

  puts payload
  puts response.body
rescue Exception => e
  puts e.message

  exit 0
end