# Datadog Metric Pipelines Pipe

Sends a custom metric to [Datadog](https://www.datadoghq.com/)

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: mid-kelola-indonesia/dd-metric-pipe:0.3.6
    variables:
      API_KEY: "<string>"
      REPOSITORY_NAME: "<string>"
      DD_METRICS: "<string|json>"
```
## Variables

| Variable             | Usage                                                       |
| -------------------- | ----------------------------------------------------------- |
| DD_METRICS (*)       | The json string of datadog key metrics |
| API_KEY              | Datadog API key. The default value will be get from DD_API_KEY env variable |
| REPOSITORY_NAME      | Set custom repository name. default will get from BITBUCKET_REPO_SLUG env |

_(*) = required variable._

### DD_METRICS JSON Attributes

| JSON Attributes | Type | Description |
| --------------- | ----------- | ----------- |
| metric | string | The datadog metric key |
| score | string | The metric score |
| tags | array | Additional tags to be sent within the metric event <br> ie: <br> `["author:mekari", "environment:production"]` |

## Prerequisites

## Examples

Basic example:

```yaml
script:
  - pipe: mid-kelola-indonesia/dd-metric-pipe:0.3.6
    variables:
      DD_METRICS: '[{"metric": "code.quality", "score": "10", "tags": ["author:mekari"]}]'
```

Advanced example:

```yaml
script:
  - pipe: mid-kelola-indonesia/dd-metric-pipe:0.3.6
    variables:
      DD_METRICS: '[{"metric": "code.quality", "score": 10, "tags": ["author:mekari"]}]'
      API_KEY: $DATADOG_API
      REPOSITORY_NAME: "platform-mekari-payment"
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, let us know.
The pipe is maintained by risal@mekari.com.

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce
