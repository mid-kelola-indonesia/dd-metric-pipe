# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.0

- minor: Initial release

## 0.2.0

- major: Change pipe language to use ruby instead

## 0.3.0

- major: change image name

## 0.3.1

- minor: update readme

## 0.3.2

- minor: update readme

## 0.3.3

- minor: add exit code when the request failed
- set no verbose when execute the pipe script

## 0.3.4

- minor: remove noecho entrypoint option

## 0.3.5

- minor: fix missmatch DD_API_KEY variable

## 0.3.5

- minor: add debuging response



